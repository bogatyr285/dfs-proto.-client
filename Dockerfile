FROM mhart/alpine-node:10

RUN mkdir -p /projects/client
WORKDIR /projects/client

COPY package.json /projects/client/package.json
COPY package-lock.json /projects/client/package-lock.json

RUN cd /projects/client \
    && echo ">> Installing NPM packages" \
    && npm i 
COPY . /projects/client

# RUN echo ">> Build project" \
#     && npm run build