import Path from 'path'
export const state = () => ({
    someField: null,
    searchField: '',
    curPath: '/',
    uploadLoadingBar: 0
})
export const actions = {
    async getFilesList({ state, commit, dispatch, error }, { path }) {
        try {
            let formData = new FormData()

            formData.append('path', path)
            let { data: files } = await this.$axios({
                url: '/dir/get',
                method: 'POST',
                headers: {
                    'Content-type': 'application/form-data'
                },
                data: formData
            })

            console.log('[getFilesList]', files.items)
            return files.items
        } catch (err) {
            returnErr(error)
        }
    },
    async downloadFile({ state, commit, dispatch, error }, { item, isInfo }) {
        try {
            let formData = new FormData()
            formData.append('path', Path.join(state.curPath, item.name))
            formData.append('info', isInfo)

            let { data:nsAnswer } = await this.$axios({
                url: '/file/get',
                method: 'POST',
                headers: {
                    'Content-type': 'application/form-data'
                },
                data: formData
            })
            console.log('[downloadFile] NS answer', nsAnswer)
            let {data:fileBlob} = await this.$axios({
                baseURL: `http://${nsAnswer.ip}/api`,
                url: `/files/download?path=${item.path}`,
                method: 'GET',
                responseType: 'blob'
            })
            const url = window.URL.createObjectURL(new Blob([fileBlob]))
            const link = document.createElement('a')
            link.href = url
            link.setAttribute('download', item.name)
            document.body.appendChild(link)
            link.click()
            console.log('[downloadFile] blob', fileBlob)
        } catch (error) {
            console.log('[downloadFile] error', error)
            returnErr(error)
        }
    },
    async uploadFile({ state, commit, dispatch, error }, { file, path, size }) {
        try {
            let formData = new FormData()
            console.log('[uploadFile] path file', path, file.name)
            formData.append('path', Path.join(path, file.name))
            formData.append('size', size)
            let { data } = await this.$axios({
                url: '/file/create',
                method: 'POST',
                headers: { 'Content-type': 'application/form-data' },
                data: formData
            })
            console.log('[uploadFile] NS answer', data)

            formData = new FormData()
            formData.append('data', file)
            formData.append('path', path)
            formData.append('id', data.id)
            data = await this.$axios({
                baseURL: 'http://' + data.ip,
                url: '/api/files/upload',
                method: 'POST',
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                data: formData,
                onUploadProgress(progressEvent) {
                    let percentage = parseInt(
                        Math.round((progressEvent.loaded * 100) / progressEvent.total)
                    )
                    commit('SET_CUR_UPLOAD_PERCENTAGE', percentage)
                }
            })
            commit('SET_CUR_UPLOAD_PERCENTAGE', 0)
            console.log('[uploadFile] Storage answer', data)
            return data.response
        } catch (error) {
            console.log('[uploadFile] error', error)
            commit('SET_CUR_UPLOAD_PERCENTAGE', 0)
            return returnErr(error)
        }
    },

    async removeFileOrFolder({ state, commit, dispatch, error }, { path }) {
        try {
            let formData = new FormData()
            formData.append('path', path)

            let { data } = await this.$axios({
                url: '/files/remove',
                method: 'POST',
                data: formData
            })
            console.log('[uploadFile] NS answer', data)
            return data.response
        } catch (error) {
            console.log('[uploadFile] error', error)
            return returnErr(error)
        }
    },

    async createFolder({ state, commit, dispatch, error }, { name, path }) {
        try {
            let formData = new FormData()
            formData.append('path', Path.join(path, name))
            let { data } = await this.$axios({
                url: '/dir/create',
                method: 'POST',
                headers: { 'Content-type': 'application/form-data' },
                data: formData
            })
            console.log('[createFolder] NS answer', data)

            formData = new FormData()
            formData.append('name', name)
            formData.append('path', path)
            formData.append('id', data.id)
            data = await this.$axios({
                baseURL: 'http://' + data.ip,
                url: '/api/files/createfolder',
                method: 'POST',
                headers: { 'Content-type': 'application/form-data' },
                data: formData
            })
            console.log('[createFolder] Storage answer', data)
        } catch (error) {
            returnErr(error)
        }
    },
    //rename or delete
    async manageFile({ state, commit, dispatch, error }, { path, isdel, newName }) {
        try {
            let formData = new FormData()
            formData.append('path', path) //folder path
            formData.append('delete', isdel)
            formData.append('new_name', newName)
            let { data } = await this.$axios({
                url: '/file/manage',
                method: 'POST',
                headers: { 'Content-type': 'application/form-data' },
                data: formData
            })
            console.log('[manageFile] NS answer', data)

            formData = new FormData()
            formData.append('name', newName)
            formData.append('path', path)
            formData.append('id', data.id)
            if (isdel) {
                data = await this.$axios({
                    baseURL: 'http://' + data.ip,
                    url: '/api/files/remove',
                    method: 'POST',
                    headers: { 'Content-type': 'application/form-data' },
                    data: formData
                })
            } else {
                data = await this.$axios({
                    baseURL: 'http://' + data.ip,
                    url: '/api/files/rename',
                    method: 'POST',
                    headers: { 'Content-type': 'application/form-data' },
                    data: formData
                })
            }
            console.log('[manageFile] Storage answer', data)
        } catch (error) {
            console.log('[manageFile] error', error)
            returnErr(error)
        }
    },

    async manageDir({ state, commit, dispatch, error }, { path, isdel, newName }) {
        try {
            let formData = new FormData()
            formData.append('path', Path.join(path, name))
            formData.append('delete', isdel)
            formData.append('new_name', newName)
            let { data } = await this.$axios({
                url: '/dir/manage',
                method: 'POST',
                headers: { 'Content-type': 'application/form-data' },
                data: formData
            })
            console.log('[manageDir] NS answer', data)

            formData = new FormData()
            formData.append('name', newName)
            formData.append('path', Path.join(path, name))
            formData.append('id', data.id)
            if (isdel) {
                data = await this.$axios({
                    baseURL: 'http://' + data.ip,
                    url: '/api/files/remove',
                    method: 'POST',
                    headers: { 'Content-type': 'application/form-data' },
                    data: formData
                })
            } else {
                data = await this.$axios({
                    baseURL: 'http://' + data.ip,
                    url: '/api/files/rename',
                    method: 'POST',
                    headers: { 'Content-type': 'application/form-data' },
                    data: formData
                })
            }
            console.log('manageDir Storage answer', data)
        } catch (error) {
            console.log('[manageDir] error', error)
            returnErr(error)
        }
    }
}

export const mutations = {
    SET_SEARCH_FIELD(state, query) {
        state.searchField = query
    },
    SET_CUR_PATH(state, path) {
        state.curPath = path
    },
    SET_CUR_UPLOAD_PERCENTAGE(state, percent) {
        state.uploadLoadingBar = percent
    }
}

export const getters = {
    getSearchField: state => {
        return state.searchField
    },
    getCurPath: state => {
        return state.curPath
    },
    getUploadPercent: state => {
        return state.uploadLoadingBar
    }
}

function returnErr(error) {

    if (typeof error.response !== 'undefined') {
        console.log('req err', error.response)
        $nuxt.error({
            statusCode: error.response.status,
            message: error.message || error.response.data.error.message
        })
    } else {
        console.log('client err', error)
        $nuxt.error({
            statusCode: error.statusCode,
            message: error.message
        })
    }
}
