# DFS.client

> Distributed File System Client. One of lab in university DS course. 
Provides interface to interract with NS and SS servers.

#### Other part of this project:
* ** Storage server: **
	* https://bitbucket.org/bogatyr285/dfs-proto.-storage-server/src/develop/
* ** Name server: **
	* https://github.com/karust/dfs_ns
	
Without others parts launching client is useless. How to launch all system was written in `storage` repository

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```